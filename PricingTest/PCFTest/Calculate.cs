﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace PCFTest
{
    [TestClass]
    public class Calculate
    {
        public IWebDriver driver;

        [TestInitialize]
        public void TestInitialize()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Url = "https://test-service.ausvdc02.pcf.dell.com/swagger/ui/index";
            WebElements.txtURL(driver).Clear();
            WebElements.txtURL(driver).SendKeys("https://test-service.ausvdc02.pcf.dell.com:/swagger/schema");
            WebElements.btnExplore(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
        }


        [TestMethod]
        [Priority(0)]
        public void CalculateShippingCharge()
        {

            WebElements.tabCalShippingCharge(driver).Click();
            Thread.Sleep(2000);
            WebElements.txtServiceCalShippingCharge(driver).SendKeys(Data.Request_Service_Data);
            WebElements.btnTryCalShippingCharge(driver).Click();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            driver.Close();
        }
    }
}
