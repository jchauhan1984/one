﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace PCFTest
{
    [TestClass]
    public class CalculateItem
    {
        public IWebDriver driver;

        [TestInitialize]
        public void TestInitialize()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Url = "https://test-service.ausvdc02.pcf.dell.com/swagger/ui/index";
            WebElements.txtURL(driver).Clear();
            WebElements.txtURL(driver).SendKeys("https://test-service.ausvdc02.pcf.dell.com:/swagger/schema");
            WebElements.btnExplore(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
        }


        [TestMethod]
        [Priority(1)]
        public void CalculateItemServiceEntitlement()
        {
            WebElements.tabCalItemSerEnt(driver).Click();
            Thread.Sleep(2000);
            WebElements.txtServiceEntitlement(driver).SendKeys(Data.Request_Service_Data);
            WebElements.btnTryCalItemSerEnt(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

        }

        [TestMethod]
        public void CalculateItemListPriceDiscount()
        {
            WebElements.tabCalItemListPriceDiscount(driver).Click();
            WebElements.txtCallItemListPriceDiscountRequest(driver).SendKeys(Data.Request_Service_Data);
            WebElements.btnTryCallItemListPriceDiscount(driver).Click();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            driver.Close();
        }
    }
}
