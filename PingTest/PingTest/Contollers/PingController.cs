﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PingTest.Contollers
{
    public class PingController : ControllerBase
    {
        //GET : ping
        /// <summary>
        ///  This is for ping
        /// </summary>
        /// <param name="sellername"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/ping")]
        public string Ping(string sellername,[FromBody] string text)
        {
            return "Pong!!";
        }
    }
}