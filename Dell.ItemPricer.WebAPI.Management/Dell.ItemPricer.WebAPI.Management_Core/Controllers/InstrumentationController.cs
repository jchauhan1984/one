﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Description;
using Dell.Commerce.Services.LoggingProviders;
using Dell.ItemPricer.WebAPI.Management_Core.Models;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.Swagger.Annotations;

namespace Dell.ItemPricer.WebAPI.Management_Core.Controllers
{
    ///<summary>
    /// WebApi controller that manages diagnostic instrumentation settings for the application.
    ///</summary>
    [RoutePrefix("instrumentation")]
    [SwaggerResponseRemoveDefaults]
    [ItemPricerLogExceptionFilter]
    public class InstrumentationController : ControllerBase
    {

        /// <summary>
        /// Returns the current service instrumentation setting.
        /// </summary>
        /// <remarks>
        /// Instrumentation is managed at the application instance. The instrumentation type currently set applies to all requests handled by this server; if the service is behind a load balancer or reverse proxy there may be more than one server instance handling requests.
        /// </remarks>
        /// <returns>InstrumentationType object.</returns>
        [HttpGet, Route("current", Name = "GetInstrumentationType"), ResponseType(typeof(InstrumentationType))]
        [SwaggerResponse(HttpStatusCode.OK, "Current service InstrumentationType.", typeof(InstrumentationType))]
        public InstrumentationType Get()
        {
            return InstrumentationManager.Get();
        }


        /// <summary>
        /// Returns a text report of the instrumentation status for the current service instance.
        /// </summary>
        /// <param name="waitTimeoutInMilliseconds"></param>
        /// <returns>Report of instrumentation status checks.</returns>
        [HttpGet, Route("report", Name = "GetReport"), ResponseType(typeof(List<string>))]
        [SwaggerResponse(HttpStatusCode.OK, "Report of instrumentation status checks.", typeof(List<string>))]
        public List<string> EventContext(int waitTimeoutInMilliseconds = 15000)
        {
            return new InstrumentationEventContext().Run(waitTimeoutInMilliseconds);
        }


        /// <summary>
        /// Sets the current instrumentation type on the application service instance.
        /// </summary>
        /// <remarks>
        /// Instrumentation is managed at the application instance. The instrumentation type currently set applies to all requests handled by this server; if the service is behind a load balancer or reverse proxy there may be more than one server instance handling requests.
        /// </remarks>
        /// <param name="value">InstrumentationType value.</param>
        /// <returns>ActionResult<Product> indicating operation PUT success.</returns>
        [HttpPut, Route("current", Name = "PutInstrumentation")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Successful response with no content.")]
        public void Put(InstrumentationType value)
        {
            InstrumentationManager.Put(value);
        }


    }
}
