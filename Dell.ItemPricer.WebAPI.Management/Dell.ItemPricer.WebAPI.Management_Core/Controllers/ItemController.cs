﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Description;
using Dell.ItemPricer.Data.DAO;
using Dell.ItemPricer.Documents.StatusReturn;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using Item = Dell.ItemPricer.Documents.Item.Item;

namespace Dell.ItemPricer.WebAPI.Management_Core.Controllers
{
    /// <summary>
    /// This controller handles all operations related to Items and Price Breaks.
    /// </summary>
    [RoutePrefix("{sellerName}")]
    [ItemPricerLogExceptionFilter]
    public class ItemController : ControllerBase
    {
        private IDataAccess _dataAccess;

        /// <summary>
        /// Default Construction of Controller.
        /// </summary>
        public ItemController()
        {
        }

        /// <summary>
        /// Controller constructor used for unit testing.
        /// </summary>
        /// <param name="dataAccess">IDataAccess object to use</param>
        public ItemController(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        /// <summary>
        /// Inserts an Item and associated Price Breaks in to the database. All necessary fields for an Item
        /// and associated Price Breaks must be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="item">JSON representation of a complete Item</param>
        /// <returns>JSON Response for Item operation and HTTP Code</returns>
        [Route("insert/item")]
        [SwaggerResponse(HttpStatusCode.Created, "Item created successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Item already exists")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.Item))]
        [HttpPost]
        public HttpResponseMessage PostItem(string sellerName, [FromBody] Item item)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var insertReturn = dao.InsertItem(item);
                return BuildHttpResponse(insertReturn.HttpCode, insertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Inserts multiple Items and associated Price Breaks into the database. The JSON represenation would be an
        /// array of Item. All necessary fields for an Item and associated Price Breaks must be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="items">JSON representation of an array of complete Items</param>
        /// <returns>JSON Response for each Item operation and HTTP Code</returns>
        [Route("insert/items")]
        [SwaggerResponse(HttpStatusCode.Created, "Items created successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Item already exists")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.Item>))]
        [HttpPost]
        public HttpResponseMessage PostItems(string sellerName, [FromBody] IEnumerable<Item> items)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var insertReturn = dao.InsertItems(items);
                return BuildHttpResponse(insertReturn.HttpCode, insertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Updates an individual item and associated Price Breaks. The Item and Price Breaks need only include required
        /// fields and those that will be updated. Fields not changing need not be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="item">JSON representation of the Item to update</param>
        /// <returns>JSON Response for Item operation and HTTP Code</returns>
        [Route("update/item")]
        [SwaggerResponse(HttpStatusCode.Accepted, "Item updated successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert\nItem to Update was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Item is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.Item))]
        [HttpPut]
        public HttpResponseMessage PutItem(string sellerName, [FromBody] Item item)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var updateReturn = dao.UpdateItem(item);
                return BuildHttpResponse(updateReturn.HttpCode, updateReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Inserts or Updates a set of Items and associated Price Breaks. This method will determine automatically
        /// whether to Insert an Item (seller item ID and seller pricelist ID combination not found) or to Update
        /// an Item (seller item ID and seller pricelist ID combination found). The rules for Insert and Update are
        /// the same here as they are for the specific Insert and Update methods.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="items">JSON representation of an array of complete Items</param>
        /// <returns>JSON Response for each Item operation and HTTP Code</returns>
        [Route("upsert/items")]
        [SwaggerResponse(HttpStatusCode.Created, "Items Upserted successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert\\Update\nItem to Update was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Item is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.Item>))]
        [HttpPost]
        public HttpResponseMessage UpsertItems(string sellerName, [FromBody] IEnumerable<Item> items)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var upsertReturn = dao.UpsertItems(items);
                return BuildHttpResponse(upsertReturn.HttpCode, upsertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Deletes an Item from the database. Does not delete any associated Price Breaks. The Delete only sets
        /// the Item to inactive and sets the Expiration Date to today. To reactivate, use Update and reset the
        /// IsActive flag and the Expiration Date.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="sellerPriceListId">Seller Price List ID of the Price List that the Item belongs to</param>
        /// <param name="sellerItemId">Seller Item ID of the Item to delete</param>
        /// <returns>JSON Response for Item operation and HTTP Code</returns>
        [Route("delete/item")]
        [SwaggerResponse(HttpStatusCode.OK, "Item deactivated successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Item to Delete was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Item is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.Item))]
        [HttpDelete]
        public HttpResponseMessage DeleteItem(string sellerName, string sellerPriceListId, string sellerItemId)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var deleteReturn = dao.DeleteItem(sellerPriceListId, sellerItemId);
                return BuildHttpResponse(deleteReturn.HttpCode, deleteReturn.OperationStatuses);
            }
        }

        private static HttpResponseMessage BuildHttpResponse(string title, string message, HttpStatusCode statusCode)
        {
            return new HttpResponseMessage(statusCode)
            {
                ReasonPhrase = title,
                Content = new StringContent(message)
            };
        }

        private static HttpResponseMessage BuildHttpResponse(HttpStatusCode httpCode, List<OperationStatus> statuses)
        {
            var serStatus = JsonConvert.SerializeObject(statuses, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            return new HttpResponseMessage(httpCode)
            {
                Content = new StringContent(serStatus, Encoding.UTF8, "application/json")
            };
        }
    }
}
