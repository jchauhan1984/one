﻿using System.Diagnostics.CodeAnalysis; 
using Dell.Commerce.Services.Core.Aspects;
using Dell.Commerce.Services.LoggingProviders; 
using PricingContract.Common; 

namespace Dell.ItemPricer.WebAPI.Management_Core.Models
{
    /// <summary>
    /// This is the service's request orchestrator class. Orchestrator will connect to various components 
    /// necessary to calculate price and invoke them.
    /// </summary>
    public class RequestOrchestrator
    { 
        [ExcludeFromCodeCoverage]
        [MethodInstrumentation(type: MethodType.EntryPoint)]
        public string HelloWorld(RequestContext context)
        {
            return "HelloWorld";
        }

       
    }
}
