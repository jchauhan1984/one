﻿using System;
using System.Collections.Generic;
using Dell.Commerce.Services.LoggingProviders;
using Dell.Commerce.Services.LoggingProviders.InstrumentationService;
using Newtonsoft.Json;
using NLog;
using PricingContract.Common;

//using CsvHelper;

namespace Dell.ItemPricer.WebAPI.Management_Core.Models
{
    /// <summary>
    /// Log handler
    /// </summary>
    public class LogHandler
    {
        private static object SyncObject = new object();
        private static Logger _instrumentationLogger=LogManager.GetLogger("InstrumentationLogger");
        private static Logger _exceptionLogger = LogManager.GetLogger("ExceptionLogger");

        /// <summary>
        /// Initialize
        /// </summary>
        public static void Initialize()
        {
            lock (SyncObject)
            {
                _instrumentationLogger = LogManager.GetCurrentClassLogger();
                //_logger.IsDebugEnabled = true;
            }
        }

        /// <summary>
        /// Write To service
        /// </summary>
        /// <param name="trees"></param>
        /// <param name="isActionable"></param>
        public static void WriteToService(List<InvocationTree> trees, bool isActionable = false)
        {
            if (_instrumentationLogger == null)
                Initialize();
            foreach (var tree in trees)
            {
                if (tree.Children != null && tree.Children.Count > 0)
                    WriteToService(tree.Children);
                
                EventContext.InformTroubleshooter(tree.RequestIdentifier, "WriteToService");
                _instrumentationLogger.Info(JsonConvert.SerializeObject(tree));
                tree.Children = null;
            }
        }

        /// <summary>
        /// Writes an exception to the logs
        /// </summary>
        /// <param name="log"></param>
        public static void WriteException(Log log)
        {
            var x =  new
            {
                log.Host,
                log.RequestIdentifier,
                log.RequestSource,
                log.RequestTime,
                log.Message,
                log.AdditionalData,
                log.LoggedMethodType,
                log.MethodName,
                log.TimingLogType,
                log.TransactionIdentifier,
                log.TransactionType,
                MethodParameters = GetParameters(log)
            };
            _exceptionLogger.Info(JsonConvert.SerializeObject(x));
        }

        /// <summary>
        ///  Global Exception Handling Log Writer
        /// </summary>
        /// <param name="ex"></param>
        public static void WriteException(Exception ex)
        {
            var x = new
            {
                Message = ex.Message,
                StackTrace = ex.StackTrace,
                ExceptionTime = DateTime.Now

            };
            _exceptionLogger.Info(JsonConvert.SerializeObject(x));
        }

        
        
        /// <summary>
        /// Maps request base to log
        /// </summary>
        /// <param name="requestBase"></param>
        /// <param name="log"></param>
        public static void MapRequestBaseToLog(RequestContext requestBase, TimingLog log)
        {
            log.RequestIdentifier = requestBase.RequestId;
            log.TransactionType = requestBase.RegistrationId.ToString();
            log.Host = requestBase.Host;
            log.RequestSource = requestBase.RequestSource;
        }
        
        //Private methods----------------------------------------

        private static Dictionary<string, string> GetParameters(Log log)
        {
            var output = new Dictionary<string, string>();
            log.MethodParameters.ForEach(parameter =>
            {
                output.Add(parameter.Key, parameter.Value.SerializedValue);
            });
            return output;
        }
    }
}