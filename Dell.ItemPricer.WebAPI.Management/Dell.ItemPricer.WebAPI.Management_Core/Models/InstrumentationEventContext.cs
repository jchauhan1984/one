﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Dell.Commerce.Services.LoggingProviders;
using Dell.Commerce.Services.LoggingProviders.InstrumentationService;
using NLog;
using NLog.Targets; 
using PricingContract.Common;

namespace Dell.ItemPricer.WebAPI.Management_Core.Models
{
    /// <summary>
    /// Sometimes it is difficult to troubleshoot why pricing service is not creating log files with instrumentation data. 
    /// To address this issue, a new web method is added to the Instrumentation controller called trouble shoot which will return the following information:
    /// 1. If the log folder exists
    /// 2. Does the user that the Pricing service is running under has permission to write/read to the log folder.
    /// 3. What methods, if any, in the instrumentation gets called when there is a request from client for the pricing services.
    /// </summary>
    public class InstrumentationEventContext
    {
        private int _timeout = 0;
        private List<string> _output = new List<string>();

        /// <summary>
        /// Run
        /// </summary>
        /// <param name="waitTimeoutInMilliseconds"></param>
        /// <returns></returns>
        public List<string> Run(int waitTimeoutInMilliseconds = 15000)
        {
            try
            {
                _output.Add("Event Context: troubleshooting started...");
                _timeout = waitTimeoutInMilliseconds;

                VerifyLogFolderExistsWithReadWritePermissions();
                VerifySampleLogEntryGetsInsertedToTheLogFile();
                VerifyAspectMethodsGetHitForATestRequest();
            }
            catch (Exception e)
            {
                _output.Add(ConvertExceptionToMessage(e));
            }
            _output.Add("--------------------------------------------------");
            _output.Add("Event Context troubleshooting finished...");
            return _output;
        }

        //Private methods---------------------------------------------------------------
        
        private void VerifyLogFolderExistsWithReadWritePermissions()
        {
            _output.Add("--------------------------------------------------");
            _output.Add(string.Format("Log folder check started..."));
            if (LogManager.Configuration == null)
            {
                _output.Add("Cannot find NLog configuration file or invalid NLog configuration section!");
            }
            else if (LogManager.Configuration.AllTargets == null)
            {
                _output.Add("No targets are specified in NLog configuration");
            }
            var targets = LogManager.Configuration.AllTargets;
            _output.Add(string.Format("NLog targets acquired. Details {0}", targets == null ? "null" : targets.Count.ToString()));
            foreach (Target target in targets)
            {
                _output.Add(string.Format("NLog target parsed."));
                var fileTarget = target as FileTarget;
                if (fileTarget != null)
                {
                    _output.Add(string.Format("NLog file target parsed."));
                    var filePath = fileTarget.FileName.ToString().Trim(' ', '\t', '\n', '\v', '\f', '\'', '"');
                    var testLogFilePath = Path.Combine(Path.GetDirectoryName(filePath), "DeleteMePlease.log");
                    using (var str = File.Create(testLogFilePath, 2,FileOptions.DeleteOnClose))
                    {
                        str.Close();
                    }
                    _output.Add(string.Format("Logfolder: Checked if the log folder exists at {0}", Path.GetDirectoryName(filePath)));
                    _output.Add(string.Format("Logfolder: Checked current users' read/write permissions by creating and deleting test log file {0}", testLogFilePath));
                }
            }
            _output.Add(string.Format("Log folder check finished..."));
        }

        private void VerifySampleLogEntryGetsInsertedToTheLogFile()
        {
            _output.Add("--------------------------------------------------");
            _output.Add(string.Format("Sample log entry check started..."));
            var invocationtree = new InvocationTree(new TimingLog
            {
                Host = Environment.MachineName,
                LoggedMethodType = MethodType.Executed,
                MethodName = "Test method",
                RequestIdentifier = Guid.NewGuid().ToString(),
                RequestSource = "Event Context",
                RequestTime = DateTime.Now,
                TimingLogType = TimingLogType.Entry,
                TransactionIdentifier = string.Empty,
                TransactionType = string.Empty
            }) { ExitTime = DateTime.Now };
            _output.Add(string.Format("Sample log request id {0}",invocationtree.RequestIdentifier));
            LogHandler.WriteToService(new List<InvocationTree> { invocationtree });
            _output.Add("Loghandler: Dummy log written to log handler");
            _output.Add(string.Format("Sample log entry check finished..."));
        }

        private void VerifyAspectMethodsGetHitForATestRequest()
        {
            _output.Add("--------------------------------------------------");
            _output.Add(string.Format("Aspect method call check started..."));
            EventContext.CaptureEvents = true;
            var currentRequestId = Guid.NewGuid().ToString();
            _output.Add(string.Format("Test Request ID: {0}", currentRequestId));
            EventContext.AddRequestToTrace(currentRequestId);
            var context = new RequestContext
            {
                RegistrationId = Guid.NewGuid(),
                RequestId = currentRequestId,
                Host = Environment.MachineName,
                RequestSource = Environment.MachineName
            };
            //var orchestrator = new RequestOrchestrator();
            //orchestrator.HelloWorld(context);
            Thread.Sleep(_timeout);
            _output.AddRange(EventContext.RetrieveAndDeleteRequestTraceLog(currentRequestId));
            EventContext.CaptureEvents = false;
            _output.Add(string.Format("Aspect method call check finished..."));
        }

        private string ConvertExceptionToMessage(Exception e)
        {
            var sb = new StringBuilder();
            sb.Append(string.Format("Message: {0}, Stack Trace: {1}", e.Message, e.StackTrace));
            if (e.InnerException != null)
            {
                sb.Append(string.Format("Inner Exception: Message: {0}, Stack Trace: {1}", e.InnerException.Message, e.InnerException.StackTrace));
            }
            return sb.ToString();
        }
    }
}