﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ItemPricer
{
    [TestClass]
    public class Item
    {
        public IWebDriver driver;

       [TestInitialize]
        public void TestInitialize()
        {
            driver = new ChromeDriver();
            driver.Url = "https://itempricerprabhu.ausvdc02.pcf.dell.com/swagger/ui/index#/";
            driver.Manage().Window.Maximize();
            Webelements.txtSwaggerUrl(driver).Clear();
            Webelements.txtSwaggerUrl(driver).SendKeys("https://itempricerprabhu.ausvdc02.pcf.dell.com:/swagger/schema");
            Webelements.btnExplore(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1000);
        }

        [TestMethod]
        public void GetSellerNamepricelistsellerPriceListIditems()
        {
            Webelements.tabGETsellerNamepricelistsellerPriceListIditems(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            Webelements.txtSllerNameGETsellerNamepricelistsellerPriceListIditems(driver).SendKeys("dell");
            Webelements.txtSellerPriceIDGETsellerNamepricelistsellerPriceListIditems(driver).SendKeys("1");
            Webelements.btnTryGETsellerNamepricelistsellerPriceListIditems(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();", Webelements.ResponseCodeGETsellerNamepricelistsellerPriceListIditems(driver));
            Assert.AreEqual(200,actualcode);
        }

        [TestMethod]
        public void POSTsellerNamepricelistsellerPriceListIditems()
        {
            Webelements.tabPOSTsellerNamepricelistsellerPriceListIditems(driver).Click();
            Webelements.txtSellerNamePOSTsellerNamepricelistsellerPriceListIditems(driver).SendKeys("dell");
            Webelements.txtPriceListIDPOSTsellerNamepricelistsellerPriceListIditems(driver).SendKeys("1");
            Webelements.txtsellerItemIdsPOSTsellerNamepricelistsellerPriceListIditems(driver).SendKeys("2");
            Webelements.btnTryPOSTsellerNamepricelistsellerPriceListIditems(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();", Webelements.ResponseCodePOSTsellerNamepricelistsellerPriceListIditems(driver));
            Assert.AreEqual(200, actualcode);
        }

        [TestMethod]
        public void GETsellerNamepricelistsellerPriceListIditemsforThisDate()
        {
            Webelements.tabGETsellerNamepricelistsellerPriceListIditemsforThisDate(driver).Click();
            Webelements.txtSellerNameGETsellerNamepricelistsellerPriceListIditemsforThisDate(driver).SendKeys("dell");
            Webelements.txtsellerPriceListIdGETsellerNamepricelistsellerPriceListIditemsforThisDate(driver).SendKeys("1");
            Webelements.txtforThisDateGETsellerNamepricelistsellerPriceListIditemsforThisDate(driver).SendKeys("2");
            Webelements.btnTryGETsellerNamepricelistsellerPriceListIditemsforThisDate(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();",Webelements.ResponseCodeGETsellerNamepricelistsellerPriceListIditemsforThisDate(driver));
            Assert.AreEqual(200, actualcode);
        }

        [TestMethod]
        public void GETsellerNamepricelistsellerPriceListIditemsfromDatetoDate()
        {
            Webelements.tabGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(driver).Click();
            Webelements.txtSellerNameGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(driver).SendKeys("dell");
            Webelements.txtsellerPriceListIdGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(driver).SendKeys("1");
            Webelements.txtfromDateGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(driver).SendKeys("2");
            Webelements.txttoDateGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(driver).SendKeys("2");
            Webelements.btnTryGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();",Webelements.ResponseCodeGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(driver));
            Assert.AreEqual(200,actualcode);
        }

        [TestCleanup]
        public void CleanUp()
        {
            driver.Close();
        }


    }
}
