﻿using Standard;
using System;

namespace Core
{
    class CoreClass
    {
        static void Main(string[] args)
        {
            StandardClass s1 = new StandardClass();
            Console.WriteLine("The addition is: " + s1.addition(2, 3));
        }
    }
}
