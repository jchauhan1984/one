﻿using Standard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    class FrameworkClass
    {
        static void Main(string[] args)
        {
            StandardClass s1 = new StandardClass();
            Console.WriteLine("The addition is: " + s1.addition(4, 5));
        }
    }
}
