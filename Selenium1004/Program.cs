using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;

namespace Sel
{
     static class Program
    {
        public static void Main(string[] args)
        {
            //Logging into the Application
            try
            {
                CommonFunction.LaunchingApplication();
                WebElements.TxtUserName(CommonFunction.driver).SendKeys(Data.user_value);
                WebElements.TxtPassword(CommonFunction.driver).SendKeys(Data.password_value);
                WebElements.BtnSubmit(CommonFunction.driver).Click();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            //Adding the Contact
            try
            {
                CommonFunction.driver.SwitchTo().Frame("mainpanel");
                Actions act = new Actions(CommonFunction.driver);
                act.MoveToElement(WebElements.TabContacts(CommonFunction.driver)).Build().Perform();
                WebElements.TabNewContact(CommonFunction.driver).Click();
                SelectElement sct = new SelectElement(WebElements.DDTitle(CommonFunction.driver));
                sct.SelectByValue(Data.title_value);
                WebElements.TextFirstName(CommonFunction.driver).SendKeys(Data.firstname_value);
                WebElements.TextLastName(CommonFunction.driver).SendKeys(Data.lastname_value);
                WebElements.UploadImage(CommonFunction.driver).SendKeys(Data.filepath_value);
                WebElements.BtnRadio(CommonFunction.driver).Click();
                CommonFunction.ScrollDown();
                WebElements.BtnDate(CommonFunction.driver).SendKeys(Data.date_task_value);
                WebElements.BtnSave(CommonFunction.driver).Click();
                string pageSource = CommonFunction.driver.FindElement(By.TagName("body")).Text;
                List<string> l1 = new List<string>();
                l1.Add("Mani");
                l1.Add("M");
                //Console.WriteLine(pageSource);
                for (int i = 0; i < l1.Count; i++)
                {
                    try
                    {
                        StringAssert.Contains(l1[i], pageSource);
                        Console.WriteLine("Page contains the value: "+l1[i]);
                    }

                    catch (Exception)
                    {
                        Console.WriteLine("Page doesn't contain the value: "+l1[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Navigating to Home Page
            try
            {
                WebElements.TabHome(CommonFunction.driver).Click();
            }

            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Searching the Contacts
            try
            {
                Actions act1 = new Actions(CommonFunction.driver);
                act1.MoveToElement(WebElements.TabContacts(CommonFunction.driver)).Build().Perform();
                WebElements.TabFullSearch(CommonFunction.driver).Click();
                SelectElement sct1 = new SelectElement(WebElements.DDSearchTitle(CommonFunction.driver));
                sct1.SelectByValue(Data.title_value);
                WebElements.TxtSearchFirstName(CommonFunction.driver).SendKeys(Data.firstname_value);
                WebElements.TxtSearchLastName(CommonFunction.driver).SendKeys(Data.lastname_value);
                WebElements.BtnSearch(CommonFunction.driver).Click();
            }

            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            int len = CommonFunction.driver.FindElements(By.XPath("//form[@id='vContactsForm']/table/tbody/tr")).Count();
            //Verifying the existing contacts in table
            try
            {
                for (int i = 4; i < len; i++)
                {
                    if (WebElements.CellSearch(CommonFunction.driver, i).Text.Equals((Data.firstname_value) + " " + (Data.lastname_value)))
                    {
                        Console.WriteLine("Value matched for RowNo:" + (i - 3));
                    }
                    else
                    {
                        Console.WriteLine("Value not matched for RowNo:" + (i - 3));
                    }
                }
            }

            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Navigating to Page: 2
            WebElements.Pagination2(CommonFunction.driver).Click();

            //Verifying the existing contacts in table:Page-2
            try
            {
                for (int i = 4; i < len; i++)
                {
                    if (WebElements.CellSearch(CommonFunction.driver, i).Text.Equals((Data.firstname_value) + " " + (Data.lastname_value)))
                    {
                        Console.WriteLine("Value matched for RowNo:" + (i + 18));
                    }
                    else
                    {
                        Console.WriteLine("Value not matched for RowNo:" + (i + 18));
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Verifying the error message
            WebElements.TabHome(CommonFunction.driver).Click();
            Actions act3 = new Actions(CommonFunction.driver);
            act3.MoveToElement(WebElements.TabContacts(CommonFunction.driver)).Build().Perform();
            WebElements.TabNewContact(CommonFunction.driver).Click();
            WebElements.BtnSave(CommonFunction.driver).Click();
            IAlert alt = CommonFunction.driver.SwitchTo().Alert();
            List<string> l2 = new List<string>();
            l2.Add(alt.Text);
            Console.WriteLine(l2[0]);
            alt.Accept();

            //Setting up new call
            try
            {
                WebElements.TabHome(CommonFunction.driver).Click();
                Actions act2 = new Actions(CommonFunction.driver);
                act2.MoveToElement(WebElements.TabCall(CommonFunction.driver)).Build().Perform();
                WebElements.TabNewCall(CommonFunction.driver).Click();
                WebElements.NewCallDate(CommonFunction.driver).Clear();
                WebElements.NewCallDate(CommonFunction.driver).SendKeys(Data.date_call_value);
                WebElements.BtnlookUpContact(CommonFunction.driver).Click();

                //Handling the Search popup
                string popupHandle = string.Empty;
                string existingwindowHandles = CommonFunction.driver.CurrentWindowHandle;
                IReadOnlyCollection<string> windowhandles = CommonFunction.driver.WindowHandles;
                foreach(string handle in windowhandles)
                {
                    if(handle!=existingwindowHandles)
                    {
                        popupHandle = handle;
                        break;
                    }
                }
                CommonFunction.driver.SwitchTo().Window(popupHandle);
                WebElements.TxtPopUpSearch(CommonFunction.driver).SendKeys(Data.PopUpSearchText_value);
                WebElements.BtnPopUpSearch(CommonFunction.driver).Click();
                WebElements.LnkSearchResult(CommonFunction.driver).Click();
                CommonFunction.driver.SwitchTo().Window(existingwindowHandles);
                CommonFunction.driver.SwitchTo().Frame("mainpanel");
                WebElements.CboxConference(CommonFunction.driver).Click();
                WebElements.btnCallSave(CommonFunction.driver).Click();
                }

            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Verifying the values in the Call Information page
            try
            {
                Assert.AreSame(WebElements.ROContact(CommonFunction.driver).Text, Data.Scheduleto_value);
                Console.WriteLine("Value are matched for schedule to value");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            try
            {
                Assert.AreSame(WebElements.ROCallScript(CommonFunction.driver).Text, Data.Callscript_value);
                Console.WriteLine("Value are matched for Call script value");
            }
            catch (Exception)
            {
                Console.WriteLine("Value not matched for Call script value");
            }





        }

    }
}
